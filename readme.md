### If you fork native post your link in our [discord](https://discord.gg/Pprt5zjv9h)

![Logo](https://i.imgur.com/VgJJ5ty.png)

# Nativegames 2.0

Nativegames is an Open Source Arcade Gaming Website

![GitHub repo size](https://img.shields.io/github/repo-size/parcoil/nativegames.net?color=33B3DB&label=Totall%20Size)

# Domains 
[The list of Domains can be found Here](https://github.com/Parcoil/nativegames.net/wiki/Domains)

## Deployment
Nativegames is powered by basic HTML and can be easily deployed on platforms like GitHub pages and Vercel.

[![Deploy with Vercel](https://vercel.com/button)](https://vercel.com/new/clone?repository-url=https%3A%2F%2Fgithub.com%2FParcoil%2Fnativegames.net)  
[![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://github.com/Parcoil/nativegames.net)

## Demo
[nativegames.net](https://nativegames.net)

## Authors
- [@Thedogecraft](https://github.com/Thedogecraft)
- [@Minoa](https://github.com/MinoaBaccus)
- [@Noober](https://github.com/Hackerman2763)
- [@3kh0](https://github.com/3kh0)
